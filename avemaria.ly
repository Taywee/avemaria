% Copyright © 2016 Taylor C. Richberger <taywee@gmx.com>
% This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
% International License. To view a copy of this license, visit
% http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
% Commons, PO Box 1866, Mountain View, CA 94042, USA.

\version "2.18.2"

\book {
  \header {
    title = "Ave Maria."
    instrument = "Violoncello."
    composer = "Fr. Schubert."
    tagline = "B. & Co 863.4311 - Lilypond typeset by Taylor C. Richberger"
    copyright = \markup { "Arrangement copyright 2016 Taylor C. Richberger " \epsfile #Y #4 #"by-sa.eps" }
  }

  \score {
    \relative c' {
      \clef "tenor"
      \time 4/4
      \tempo "Lento assai."
      \compressFullBarRests
      R1 |
      \repeat volta 2 {
        R1 |
        c4.\p\< b16( c\!) e4..\>( d16\!) |
        c4 r d-2 \grace { e16( d } c16-2)( b a b) |
        c4 r8 e8-4 e8.( d32 c-2) b16\<( a e'16.-2 fis32) |
        e4\!( dis8.) b16-1( d8.\> c16) \tuplet 3/2 { b16( d-1 e f d b) } |
        c4.\! e16-4( d) d8.-4\<( b16) \tuplet 3/2 { a16( cis e-1 g e cis-1) } | 
        d4~\! \tuplet 3/2 { d16[\> a-0( b c \grace { d c } b a\!]) } g4 r8 g\p\upbow |
        d'8.-2~ d16-. d16.( cis32) d16.( e32) d16.( e32) c8 r c |
        d8.-2~ d16 \tuplet 3/2 { d16( cis d-1 f e d) } c4 r8 c |
        d8.-1\<~ d16-.   e16.~ e32\!-. \tuplet 3/2 { e16\prall[( d e-1]) } g16\>( f f8--\!) r8. a,16( |
        e'16\p d8) d16( \tuplet 3/2 { c16 b c-1 es d c) } d4. r8 |
        c4.\downbow\p\< b16( c\!) e4..\>( d16\!) |
        c4 r r2 |
      }
      R1*2 |
      r1\fermata \bar "|." |
    }

    \layout {}
    \midi {}
  }
}
